package com.texoit.goldenraspberryawards.test.producers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.texoit.goldenraspberryawards.producer.ProducerController;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations="classpath:application-test.properties")
public class ProducerIntegrationTests {

	private MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext applicationContext;

	@Autowired
	private ProducerController producerController;
	
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(producerController).build();
	}
	
	@Test
	public void testGetAllProducers() throws Exception {
		MvcResult result = mockMvc.perform(get("/producers/").contentType(MediaType.APPLICATION_JSON))
	            .andDo(print())
	            .andExpect(status().isOk())
	            .andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		Assert.assertTrue(content.contains("Allan Carr")); // Primeiro
		Assert.assertTrue(content.contains("Steven Perry")); // Meio
		Assert.assertTrue(content.contains("Lorenzo di Bonaventura")); // Ultimo
		
		
	}

}