package com.texoit.goldenraspberryawards.test.movies;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.web.context.WebApplicationContext;

import com.texoit.goldenraspberryawards.movies.MovieController;

@RunWith(SpringRunner.class)
@SpringBootTest
@TestPropertySource(locations="classpath:application-test.properties")
public class MovieIntegrationTests {

	private MockMvc mockMvc;

	@Autowired
	protected WebApplicationContext applicationContext;

	@Autowired
	private MovieController movieController;
	
	
	@Before
	public void setup() throws Exception {
		this.mockMvc = standaloneSetup(movieController).build();
	}
	
	@Test
	public void testGetAllMovies() throws Exception {
		MvcResult result = mockMvc.perform(get("/movies/").contentType(MediaType.APPLICATION_JSON))
	            .andDo(print())
	            .andExpect(status().isOk())
	            .andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		Assert.assertTrue(content.contains("Can't Stop the Music")); // Primeiro
		Assert.assertTrue(content.contains("Tarzan")); // Meio
		Assert.assertTrue(content.contains("Transformers: The Last Knight")); // Ultimo
		
		
	}

	@Test
	public void testIntervalMinMaxOriginalFile() throws Exception {
		MvcResult result = mockMvc.perform(get("/movies/interval").contentType(MediaType.APPLICATION_JSON))
	            .andDo(print())
	            .andExpect(status().isOk())
	            .andReturn();
		
		String content = result.getResponse().getContentAsString();
		
		Assert.assertTrue(content.contains("{\"min\":{\"producer\":\"Joel Silver\",\"interval\":1,\"previousWin\":1990,\"followingWin\":1991}"));
		Assert.assertTrue(content.contains("\"max\":{\"producer\":\"Matthew Vaughn\",\"interval\":13,\"previousWin\":2002,\"followingWin\":2015}"));
	}
	
}