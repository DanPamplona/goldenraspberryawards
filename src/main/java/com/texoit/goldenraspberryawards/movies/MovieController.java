package com.texoit.goldenraspberryawards.movies;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.goldenraspberryawards.AbstractController;

@RestController
@RequestMapping("movies")
public class MovieController extends AbstractController<Movie, MovieService> {
	
	@GetMapping(path = "/", produces = "application/json")
	public @ResponseBody List<Movie> getAllMovies() {
		return getAll();
	}
	
	@GetMapping(path = "/interval", produces = "application/json")
	public @ResponseBody MinMaxIntervalDTO getMinMaxInterval() {
		return service.getMinMaxInterval();
	}

}
