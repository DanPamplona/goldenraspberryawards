package com.texoit.goldenraspberryawards.movies;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProducerWinDTO {

	private String producer;
	private int win;
}
