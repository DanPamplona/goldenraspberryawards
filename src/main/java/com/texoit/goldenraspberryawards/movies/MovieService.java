package com.texoit.goldenraspberryawards.movies;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.springframework.stereotype.Service;

import com.texoit.goldenraspberryawards.AbstractService;
import com.texoit.goldenraspberryawards.producer.Producer;

@Service
public class MovieService extends AbstractService<Movie, MovieRepository> {

	public MinMaxIntervalDTO getMinMaxInterval() {
		List<Long> allProducersWithTwoWinsOrMore = repository.getAllProducersWithTwoWinsOrMore();
		List<Movie> movies = repository.getWinnerMoviesByProducers(allProducersWithTwoWinsOrMore);
		List<ProducerWinDTO> winnerProducersByYear = new ArrayList<ProducerWinDTO>();
		List<ProducerWinIntervalDTO> allWinIntervalList = new ArrayList<ProducerWinIntervalDTO>();
		for (Movie movie : movies) {
			for (Producer prod : movie.getProducers()) {
				winnerProducersByYear.add(new ProducerWinDTO(prod.getName(), movie.getYear()));
			}
		}
		for (ProducerWinDTO producerYear : winnerProducersByYear) {
			ProducerWinIntervalDTO intervalDTO = getProducerWinIntervalInList(allWinIntervalList, producerYear.getProducer());
			if (intervalDTO == null) {
				intervalDTO = new ProducerWinIntervalDTO(producerYear.getProducer(), null, null, producerYear.getWin());
				allWinIntervalList.add(intervalDTO);
			} else {
				if (producerYear.getWin() > intervalDTO.getFollowingWin()) { 
					intervalDTO.setPreviousWin(intervalDTO.getFollowingWin());
					intervalDTO.setFollowingWin(producerYear.getWin());
				} else {
					intervalDTO.setPreviousWin(Math.max(intervalDTO.getPreviousWin(), producerYear.getWin()));
				}
				intervalDTO.setInterval(intervalDTO.getFollowingWin() - intervalDTO.getPreviousWin());
			}
		}
		
		allWinIntervalList.removeIf(intervalDTO -> intervalDTO.getInterval() == null);
		allWinIntervalList.sort(Comparator.comparingInt(ProducerWinIntervalDTO::getInterval));
				
		return new MinMaxIntervalDTO(allWinIntervalList.get(0), allWinIntervalList.get(allWinIntervalList.size() - 1));
	}
	
	private ProducerWinIntervalDTO getProducerWinIntervalInList(List<ProducerWinIntervalDTO> intervalListDTO, String producerName) {
		return intervalListDTO.stream().filter(interval -> interval.getProducer().equalsIgnoreCase(producerName)).findAny().orElse(null);
	}

}
