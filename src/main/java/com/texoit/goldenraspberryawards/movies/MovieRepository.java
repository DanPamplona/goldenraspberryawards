package com.texoit.goldenraspberryawards.movies;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface MovieRepository extends CrudRepository<Movie, Long> {

	@Query(" SELECT producer.id "
			+ " FROM Movie AS movie "
			+ " JOIN movie.producers AS producer "
			+ " WHERE movie.winner = 1 AND (SELECT COUNT(m.id) FROM Movie AS m JOIN m.producers AS p WHERE p.id = producer.id AND m.winner = 1) > 1"
			+ " GROUP BY producer")
	public List<Long> getAllProducersWithTwoWinsOrMore();
	
	
	@Query(" SELECT movie FROM Movie movie "
			+ " JOIN movie.producers AS producer "
			+ " WHERE movie.winner = 1 AND producer.id IN (?1)")
	public List<Movie> getWinnerMoviesByProducers(List<Long> idsProducers);

}
