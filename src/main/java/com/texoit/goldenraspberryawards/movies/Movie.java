package com.texoit.goldenraspberryawards.movies;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.texoit.goldenraspberryawards.AbstractEntity;
import com.texoit.goldenraspberryawards.producer.Producer;
import com.texoit.goldenraspberryawards.studio.Studio;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Movie")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Movie extends AbstractEntity {
	
	@Column
	private int year;
	
	@Column
	private String title;

	@ManyToMany(fetch = FetchType.EAGER)
	private List<Studio> studios;

	@ManyToMany(fetch = FetchType.EAGER)
	private List<Producer> producers;
	
	@Column
	private boolean winner;
	

}
