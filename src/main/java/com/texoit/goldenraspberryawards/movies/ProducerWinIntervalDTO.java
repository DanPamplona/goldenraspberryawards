package com.texoit.goldenraspberryawards.movies;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProducerWinIntervalDTO {

	private String producer;
	private Integer interval;
	private Integer previousWin;
	private Integer followingWin;
}
