package com.texoit.goldenraspberryawards.movies;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MinMaxIntervalDTO {

	private ProducerWinIntervalDTO min;
	private ProducerWinIntervalDTO max;
}
