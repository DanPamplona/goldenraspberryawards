package com.texoit.goldenraspberryawards;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.texoit.goldenraspberryawards.movies.Movie;
import com.texoit.goldenraspberryawards.movies.MovieRepository;
import com.texoit.goldenraspberryawards.producer.Producer;
import com.texoit.goldenraspberryawards.producer.ProducerRepository;
import com.texoit.goldenraspberryawards.studio.Studio;
import com.texoit.goldenraspberryawards.studio.StudioRepository;

@Component
public class DatabaseInit {
	
	@Value("${database.filename}")
	private String fileName;
	
	@Autowired
	private MovieRepository movieRepository;

	@Autowired
	private StudioRepository studioRepository;
	
	@Autowired
	private ProducerRepository producerRepository;
	
	
	@PostConstruct
	private void postConstruct() {
		File file = new File(getClass().getClassLoader().getResource(fileName).getFile());
		if (file.exists()) {
			try (FileReader fr = new FileReader(file); BufferedReader br = new BufferedReader(fr)) {
				String line = null;
				boolean firstLine = true;
				while ((line = br.readLine()) != null) {
					if (!firstLine) {
						persistMovie(line);
					}
					firstLine = false;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	

	@Transactional
	private void persistMovie(String lineReaded) {
		Movie movie = new Movie();
		
		String[] info = lineReaded.split(";");
		int idx = 0;
		movie.setYear(Integer.valueOf(info[idx++]));
		movie.setTitle(info[idx++]);
		idx = readStudiosList(info, idx, movie);
		idx = readProducersList(info, idx, movie);
		movie.setWinner(info.length == 5);
		
		movieRepository.save(movie);
	}

	private int readStudiosList(String[] info, int idx, Movie movie) {
		List<Studio> studios = new ArrayList<Studio>();
		String[] studiosStr = info[idx++].split(",");
		Stream.of(studiosStr).forEach(studioName -> {
			studioName = studioName.trim();
			Studio studio = studioRepository.findByName(studioName).orElse(new Studio(studioName));
			if (studio.getId() == null) {
				studio = studioRepository.save(studio);
			}
			studios.add(studio);
		});
		movie.setStudios(studios);
		return idx;
	}
	
	private int readProducersList(String[] info, int idx, Movie movie) {
		List<Producer> producers = new ArrayList<Producer>();
		String[] producerStr = info[idx++].split(",| and ");
		Stream.of(producerStr).forEach(producerName -> {
			producerName = producerName.trim();
			Producer producer = producerRepository.findByName(producerName).orElse(new Producer(producerName));
			if (producer.getId() == null) {
				producer = producerRepository.save(producer);
			}
			producers.add(producer);
		});
		movie.setProducers(producers);
		return idx;
	}

}
