package com.texoit.goldenraspberryawards;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public abstract class AbstractController<E extends AbstractEntity, S extends AbstractService<E, ? extends CrudRepository<E, Long>>> {

	@Autowired
	protected S service;
	
	public List<E> getAll() {
		return service.getAll();
	}
	
	public E getById(Long id) {
		return service.getById(id);
	}
}
