package com.texoit.goldenraspberryawards.producer;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface ProducerRepository extends CrudRepository<Producer, Long> {
	
	@Query("SELECT p FROM Producer p WHERE p.name = ?1")
	public Optional<Producer> findByName(String name);
	
}
