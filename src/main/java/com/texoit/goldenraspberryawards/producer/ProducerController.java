package com.texoit.goldenraspberryawards.producer;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.goldenraspberryawards.AbstractController;

@RestController
@RequestMapping("producers")
public class ProducerController extends AbstractController<Producer, ProducerService> {
	
	@GetMapping(path = "/", produces = "application/json")
	public @ResponseBody List<Producer> getAllProducers() {
		return getAll();
	}

}
