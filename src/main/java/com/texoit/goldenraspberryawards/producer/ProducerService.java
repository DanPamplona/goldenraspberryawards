package com.texoit.goldenraspberryawards.producer;

import org.springframework.stereotype.Service;

import com.texoit.goldenraspberryawards.AbstractService;

@Service
public class ProducerService extends AbstractService<Producer, ProducerRepository> {

}
