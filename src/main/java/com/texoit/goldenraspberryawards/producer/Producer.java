package com.texoit.goldenraspberryawards.producer;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.texoit.goldenraspberryawards.AbstractEntity;
import com.texoit.goldenraspberryawards.movies.Movie;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Producer")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Producer extends AbstractEntity {
	
	public Producer(String name) {
		this.name = name;
	}
	
	@Column
	private String name;
	

	@ManyToMany(fetch = FetchType.EAGER)
	@JsonIgnore
	private List<Movie> movie;
}
