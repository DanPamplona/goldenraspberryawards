package com.texoit.goldenraspberryawards.studio;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.texoit.goldenraspberryawards.AbstractController;

@RestController
@RequestMapping("studios")
public class StudioController extends AbstractController<Studio, StudioService> {
	
	@GetMapping(path = "/", produces = "application/json")
	public @ResponseBody List<Studio> getAllStudios() {
		return getAll();
	}

}
