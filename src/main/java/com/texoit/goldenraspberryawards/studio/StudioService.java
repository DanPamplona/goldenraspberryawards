package com.texoit.goldenraspberryawards.studio;

import org.springframework.stereotype.Service;

import com.texoit.goldenraspberryawards.AbstractService;

@Service
public class StudioService extends AbstractService<Studio, StudioRepository> {

}
