package com.texoit.goldenraspberryawards.studio;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.texoit.goldenraspberryawards.AbstractEntity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "Studio")
@Data
@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
public class Studio extends AbstractEntity {
	
	@Column
	private String name;
	
}
