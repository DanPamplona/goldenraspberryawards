package com.texoit.goldenraspberryawards.studio;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Component;

@Component
public interface StudioRepository extends CrudRepository<Studio, Long> {
	
	@Query("SELECT s FROM Studio s WHERE s.name = ?1")
	public Optional<Studio> findByName(String name);

}
