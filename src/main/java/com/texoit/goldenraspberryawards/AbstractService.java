package com.texoit.goldenraspberryawards;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public abstract class AbstractService<E extends AbstractEntity, R extends CrudRepository<E, Long>> {

	@Autowired
	protected R repository;
	
	public List<E> getAll() {
		List<E> convertedList = new ArrayList<E>();
		Iterable<E> findAll = repository.findAll();
		findAll.forEach(convertedList::add);
		return convertedList;
	}
	
	public E getById(Long id) {
		return repository.findById(id).get();
	}
}
