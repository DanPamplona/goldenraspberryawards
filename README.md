# READ-ME - Projeto - Daniel Pamplona Soares

### Rodar por fora do Eclipse
1 - Garantir que a vari�vel de ambiente JAVA_HOME est� apontando pra uma JDK 1.8

2 - Ir pelo CMD at� a pasta ra�z do projeto (onde est� o arquivo gradlew)

3 - Para rodar o sistema, executar:  `gradlew bootRun`

3.1 - Ap�s inicializar, para testar o retorno, abrir http://localhost:8080/movies/interval

4 - Para rodar os testes, executar:  `gradlew test --rerun-tasks`

Obs: O arquivo da base est� salvo em src/main/resources/static

### Rodar pelo Eclipse
Para rodar o sistema pelo eclipse, � necess�ria a instala��o do plugin do Lombok.

Baixar o jar http://central.maven.org/maven2/org/projectlombok/lombok/1.18.8/

Executar o .jar, esperar identificar o Eclipse instalado e depois clicar em Install / Update

Quit Install

Reiniciar Eclipse